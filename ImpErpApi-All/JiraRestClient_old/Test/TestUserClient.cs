using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NetJira.Test
{
    [TestClass]
    public class TestUserClient : BaseTest
    {

        [TestMethod]
        public void testGetLoggedInUser()
        {
            var userclient = restClient.UserClient;
            var task = userclient.GetLoggedInUser();
            var user = task.GetAwaiter().GetResult();
            Assert.IsNotNull(user);
        }

        [TestMethod]
        public void TestGetUserByUsername()
        {
            var userclient = restClient.UserClient;
            var task = userclient.GetUserByUsername(username);
            var user = task.GetAwaiter().GetResult();
            Assert.IsNotNull(user);
        }
    }
}