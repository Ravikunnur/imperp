using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using ImpErpApi.Core;


namespace ImpErpApi
{
    public class JiraRestClient
    {
        private string _username;

        private readonly HttpClient _client;

        private readonly Uri _baseUri;

        private IssueClient _issueClient;     

        private ProjectClient _projectClient;

        public JiraRestClient(Uri uri, string username, string password)
        {
            this._client = new HttpClient();
            this._baseUri = uri;
            this._username = username;
            var bytes = Encoding.ASCII.GetBytes(username + ":" + password);
            var token = Convert.ToBase64String(bytes);           
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
        }

        public IssueClient IssueClient
        {
            get
            {
                if (_issueClient == null)
                {
                    _issueClient = new IssueClient(this);
                }
                return _issueClient;
            }
        }


        public ProjectClient ProjectClient
        {
            get
            {
                if (_projectClient == null)
                {
                    _projectClient = new ProjectClient(this);
                }
                return _projectClient;
            }
        }

        public HttpClient Client
        {
            get
            {
                return _client;
            }
        }

        public Uri BaseUri
        {
            get
            {
                return _baseUri;
            }
        }

        public string Username
        {
            get
            {
                return _username;
            }
        }
    }

}
