﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpErpApi.Domain
{
    public class ResultModel
    {
        public bool IsSuccess { get; set; }

        public dynamic Data { get; set; }

        public int Identity { get; set; }

        public string Message { get; set; }

        public string StatusCode { get; set; }
    }
}
