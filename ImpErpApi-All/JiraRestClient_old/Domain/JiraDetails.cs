﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpErpApi.Domain
{
    public class JiraDetails
    {
        public string JiraUrl { get; set; }
        public string JiraUserName { get; set; }
        public string JiraPassword { get; set; }
        public string JiraBoardId { get; set; }
    }
}
