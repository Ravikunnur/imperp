﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpErpDB.Data
{
    public interface IDbInitializer 
    {
        void Initialize();
    }
}
