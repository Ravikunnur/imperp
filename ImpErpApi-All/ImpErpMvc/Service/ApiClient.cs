﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ImpErpMvc.Models;
using Newtonsoft.Json;
using ImpErpCommon.Models;

namespace ImpErpMvc.Service
{
    public class ApiClient
    {
        private readonly HttpClient _client;
        private ResultModel resultModel;
        private string _baseUri;

        private Uri _uri;
        public ApiClient()
        {
            _client = new HttpClient();
            _baseUri = "https://localhost:44350/jira/";            
            resultModel = new ResultModel();
        }

        public string GetAllProject()
        {
            _baseUri = string.Concat(_baseUri,"getallprojects");
            _uri = new Uri(_baseUri);
            var request = new HttpRequestMessage(HttpMethod.Get, _uri);           

            var task = _client.SendAsync(request);
            var resp = task.GetAwaiter().GetResult();

            var respstr = resp.Content.ReadAsStringAsync().Result;

            return respstr;
        }
    }
}
