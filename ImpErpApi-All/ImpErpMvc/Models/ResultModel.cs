﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImpErpMvc.Models
{
    public class ResultModel
    {
        public bool IsSuccess { get; set; }

        public dynamic Data { get; set; }

        public int Identity { get; set; }

        public string Message { get; set; }

        public string StatusCode { get; set; }
    }
}
