using System.Threading.Tasks;
using ImpErpCommon.Models;
using ImpErpApi.Util;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ImpErpApi.Core
{
    /// <summary>
    /// Client to get Issues
    /// </summary>
    public class IssueClient : BaseClient
    {
        private ResultModel result;
        public IssueClient(JiraRestClient jiraRestClient) : base(jiraRestClient)
        {
            result = new ResultModel();
            result.IsSuccess = false;
        }

        /// <summary>
        /// Get a Issue by key
        /// </summary>
        /// <param name="key">The key of the Issue</param>
        /// <returns>A async Task containing the Issue</returns>
        public async Task<Issue> GetIssueByKey(string key)
        {
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.ISSUE, key);
            var completeUri = restUriBuilder.ToString();
            var stream = client.GetStreamAsync(completeUri);
            var serializer = new DataContractJsonSerializer(typeof(Issue));
            return serializer.ReadObject(await stream) as Issue;
        }


        public async Task<Issue> GetIssueByKey(string key, List<string> fields, List<string> expand)
        {
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.ISSUE, key);
            if(fields != null && fields.Count > 0)
            {
                var fieldsParam = string.Join(",", fields);
                UriHelper.AddQuery(restUriBuilder, RestParamConstants.FIELDS, fieldsParam);
            }
            if(expand != null && expand.Count > 0)
            {
                var expandParam = string.Join(",", expand);
                UriHelper.AddQuery(restUriBuilder, RestParamConstants.EXPAND, expandParam);
            }
            var completeUri = restUriBuilder.ToString();
            var stream = client.GetStreamAsync(completeUri);
            var serializer = new DataContractJsonSerializer(typeof(Issue));
            return serializer.ReadObject(await stream) as Issue;
        }

        /// <summary>
        ///  This is a helper method to create meta data
        /// </summary>
        /// <param name="projectKeys"></param>
        /// <returns></returns>
        public async Task<ResultModel> CreateMeta(string projectKeys)
        {
            //http://kelpie9:8081/rest/api/2/issue/createmeta?projectKeys=QA&issuetypeNames=Bug&expand=projects.issuetypes.fields
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.ISSUE, "createmeta");
            UriHelper.AddQuery(restUriBuilder, RestParamConstants.PROJECTKEYS, projectKeys);
            var completeUri = restUriBuilder.ToString();
            completeUri = string.Concat(completeUri, "&issuetypeNames=Bug&expand=projects.issuetypes.fields");
            HttpRequestMessage httpMsg = new HttpRequestMessage(HttpMethod.Get, completeUri);

            HttpResponseMessage resp = client.SendAsync(httpMsg).Result;
            var r = resp.Content.ReadAsStringAsync();            
            result.Data = r;         
            return result;
        }

        public async Task<ResultModel> CreateIssue(StringContent createIssueJson)
        {
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.ISSUE);
           // createIssueJson.Headers.Add("Content-Type", "application/json");
            var completeUri = restUriBuilder.ToString();
            var resp = await client.PostAsync(completeUri, createIssueJson);
            result.Data = resp.Content.ReadAsStringAsync();
            result.IsSuccess = true;

            return result;
        }
    }
}