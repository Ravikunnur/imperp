﻿using System.Collections.Generic;

namespace ImpErpCommon.Models
{
    public class Project :Key
    {
        public string expand { get; set; }
        public string description { get; set; }
        public User lead { get; set; }
        public List<Component> components { get; set; }
        public List<IssueType> issueTypes { get; set; }
        public string assigneeType { get; set; }
        public List<Version> versions { get; set; }
        public Roles roles { get; set; }
        public AvatarUrls avatarUrls { get; set; }
    }

    public class Roles
    {
        public string Users { get; set; }
        public string Administrators { get; set; }
        public string Developers { get; set; }
    }

    public class HubSpotDeals
    {
        public string PortalId { get; set; }
        public string DealId { get; set; }
        public Dictionary<string,string> Properties { get; set; }

    }


    public class TempoWorkLogs
    {
        public string WorkLogId { get; set; }
        public string IssueId { get; set; }
        public string IssueKey { get; set; }
        public string IssueDesc { get; set; }
        public string ProjectKey { get; set; }
        public string ProjectName { get; set; }
        public string UserAccountId { get; set; }
        public string DisplayName { get; set; }
        
      
        public int TimeSpentInHrs { get; set; }
        public int BillableHrs { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }

    }
    public class HubSpotProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
      

    }

    public class HubSpotVersion
    {
        public string name { get; set; }
        public string value { get; set; }
        public string source { get; set; }
        public string sourceId { get; set; }
    }
}
