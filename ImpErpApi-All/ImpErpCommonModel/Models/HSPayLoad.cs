﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpErpCommon.Models
{
    public class HSPayLoad
    {/*
        "objectId": 1246965,
    "propertyName": "lifecyclestage",
    "propertyValue": "subscriber",
    "changeSource": "ACADEMY",
    "eventId": 3816279340,
    "subscriptionId": 25,
    "portalId": 33,
    "appId": 1160452,
    "occurredAt": 1462216307945,
    "subscriptionType": "contact.propertyChange",
    "attemptNumber": 0*/

        public string objectId { get; set; }
        public string propertyName { get; set; }
        public string propertyValue { get; set; }
        public string changeSource { get; set; }
        public long eventId { get; set; }
        public int subscriptionId { get; set; }
        public int portalId { get; set; }
        public long appId { get; set; }
        public long occurredAt { get; set; }
        public string subscriptionType { get; set; }
        public int attemptNumber { get; set; }
    }
}
