﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpErpApi.Domain
{
    public class JiraDetails
    {
        public string JiraUrl { get; set; }
        public string JiraUserName { get; set; }
        public string JiraPassword { get; set; }
        public string JiraBoardId { get; set; }
    }

    public class JiraTempoDetails
    {
        public string JiraTempoUrl { get; set; }
        public string JiraTempoUserName { get; set; }
        public string JiraTempoPassword { get; set; }
        
    }

    public class HubSpotDetails
    {
        public string HubSpotUri { get; set; }
        public string HubSpotApiToken { get; set; }
        public string AppId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public string OauthUrl { get; set; }
        public string TokenEndpoint { get; set; }
        public string Scope { get; set; }
        public string RedirectUrl { get; set; }

    }
}
