﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ImpErpCommon.Models
{
    public class ProjectModel
    {
        public string ProjectName { get; set; }
        public string Key { get; set; }
        public long Id { get; set; }
    }
}
