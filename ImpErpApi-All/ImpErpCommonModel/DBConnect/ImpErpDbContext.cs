﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace ImpErpCommon.DBConnect
{
    public class ImpErpDbContext
    {
        public string ConnectionString { get; set; }

        public ImpErpDbContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

    }
}
