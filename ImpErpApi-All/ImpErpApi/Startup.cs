using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ImpErpApi.Domain;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.OpenApi;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using ImpErpCommon.DBConnect;

namespace ImpErpApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = "Hubspot";
            })
               
              .AddCookie()
              .AddOAuth("Hubspot", options =>
              {
                  options.ClientId = Configuration["Hubspot:ClientId"];
                  options.ClientSecret = Configuration["Hubspot:ClientSecret"];
                  options.CallbackPath = new PathString("/Home");

                  options.AuthorizationEndpoint = Configuration["Hubspot:OauthUrl"];
                  options.TokenEndpoint = Configuration["Hubspot:TokenEndpoint"];
                  options.Scope.Clear();
                  options.Scope.Add(Configuration["Hubspot:Scope"]);
                  options.SaveTokens = true;

              });

            services.Configure<JiraDetails>(Configuration.GetSection("JiraLogin"));
            services.Configure<JiraTempoDetails>(Configuration.GetSection("JiraTempoLogin"));
            services.Configure<HubSpotDetails>(Configuration.GetSection("HubSpotDetails"));
            services.Add(new ServiceDescriptor(typeof(ImpErpDbContext), new ImpErpDbContext(Configuration.GetConnectionString("DefaultConnection"))));
            //Swagger Implementation


            services.AddSwaggerGen(c =>
            {
              
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "ImpErpApi - Impiger ERP Api",
                    Description = "Used by web portal",
                     Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                     {
                         Name = "Impiger",
                         Url = new Uri("http://impigertech.com")
                     }
                });             

            });
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Impiger ERP - API V1");
            });
        }
    }
}
