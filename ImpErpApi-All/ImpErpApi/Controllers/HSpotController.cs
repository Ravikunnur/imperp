﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ImpErpApi.Domain;
using ImpErpCommon.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ImpErpApi;

namespace ImpErpApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HSpotController : ControllerBase
    {

        private HubSpotClient restClient;
        private IOptions<HubSpotDetails> _hubSpotDetails;
        private ResultModel resultModel;
        public HSpotController(IOptions<HubSpotDetails> hubSpotDetails)
        {
            _hubSpotDetails = hubSpotDetails;
            string  url = hubSpotDetails.Value.HubSpotUri;
            var apiToken = hubSpotDetails.Value.HubSpotApiToken;  //_configurationRoot["JiraPassword"];
            restClient = new HubSpotClient(url, apiToken);
            resultModel = new ResultModel();
        }
       
        [HttpGet]
        [Route("getdealbyid")]
        public ResultModel GetDeal(int dealId=1)
        {
            var resp = restClient.GetDealById(dealId).Result;
            return resp;
        }


        [HttpGet]
        [Route("getpreviousdaydeal")]
        public ResultModel GetPreviousdayDeal()
        {
            var resp = restClient.GetPreviousdayDeal().Result;
            return resp;
        }


        [HttpGet]
        [Route("getwondeals")]
        public ResultModel GetWonDeals()
        {
            var resp = restClient.GetWonDeals().Result;
            return resp;
        }
        //public async Task<ResultModel> GetDeals_old()
        //{
        //    FormUrlEncodedContent content = new FormUrlEncodedContent(authenticationCredentials);

        //    HttpResponseMessage response = await httpClient.PostAsync("https://api.hubapi.com/oauth/v1/token", content);
        //    string responseString = await response.Content.ReadAsStringAsync();

        //    Token token = JsonConvert.DeserializeObject<Token>(responseString);

        //    return resultModel;

        //}
    }
}