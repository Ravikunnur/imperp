﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ImpErpApi.Core;
using ImpErpCommon.Models;
using ImpErpApi;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using ImpErpApi.Domain;

namespace ImpErpApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class JiraController : ControllerBase
    {
        private JiraRestClient restClient;
        private IOptions<JiraDetails> _jiraDetails;
        private ResultModel resultModel;
        public JiraController(IOptions<JiraDetails> jiraDetails)
        {
            _jiraDetails = jiraDetails;
            Uri url = new Uri(_jiraDetails.Value.JiraUrl);
            var username = _jiraDetails.Value.JiraUserName; //_configurationRoot["JiraUsername"];
            var password = _jiraDetails.Value.JiraPassword;  //_configurationRoot["JiraPassword"];

            restClient = new JiraRestClient(url, username, password);

            resultModel = new ResultModel();
        }

        [HttpPost]
        [Route("createproject")]
        public ResultModel CreateProject()
        {
            var post_params = @"{ ""key"": ""EX"",
                                ""name"": ""Example"",
                                ""projectTypeKey"": ""business"",
                                ""projectTemplateKey"": ""com.atlassian.jira-core-project-templates:jira-core-project-management"",
                                ""description"": ""Example Project description"",
                                ""lead"": ""ravikunnur"",
                                ""url"": ""https://raviramjira.atlassian.net:443/"",      
                                ""avatarId"": 10200
                                }";
            var content = new StringContent(post_params, Encoding.UTF8, "application/json");
            var resp = restClient.ProjectClient.CreateProject(content);
            var result = resp.GetAwaiter().GetResult();
            resultModel.Data = result;
            return resultModel;
        }

        [HttpGet]
        [Route("getallprojects")]
        public List<ProjectModel> GetProjects()
        {
            var task = restClient.ProjectClient.GetAllProjects();
            var resp = task.GetAwaiter().GetResult();
            var projectList = new List<ProjectModel>();

            foreach (var prj in resp)
            {
                var project = new ProjectModel
                {
                    ProjectName = prj.name,
                    Key = prj.key,
                    Id = prj.id
                };
                projectList.Add(project);
            }
          return projectList;
        }        

        [HttpPost]
        [Route("createissue")]
        public ResultModel CreateIssue(string issu)
        {
            var post_params = "{\"fields\": {\"project\": {\"key\": \"AUGRES\"},\"summary\": \"Test Jira integration\",\"issuetype\": {\"name\": \"Bug\",\"subtask\":false},\"customfield_11600\":  {\"value\":\"Internal\",\"id\":\"10901\"},\"customfield_10700\":  {\"value\":\"Minor\",\"id\":\"10103\"}  } }";
            var content = new StringContent(post_params, Encoding.UTF8, "application/json");
            var resp = restClient.IssueClient.CreateIssue(content);
            var result = resp.GetAwaiter().GetResult();
            resultModel.Data = result;
            return resultModel;
        }

        [HttpGet]
        [Route("getaccountid")]
        public ResultModel GetAccountId()  //557058:4c974ba7-bc46-4c73-b600-cfc738633b4c
        {
            var resp = restClient.ProjectClient.GetAccountId();
            var result = resp.GetAwaiter().GetResult();
            resultModel.IsSuccess = true;
            resultModel.Data = result;
            return resultModel;

        }

        
    }
}