﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace ImpErpConsole
{
    class Program
    {
        static void Main(string[] args)
        {


            //This part to be moved to Aji machine
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            var hubspotClient = new HubSpotClient(config);
            hubspotClient.GetWonDeals();
           // Console.ReadKey();
            

            // 
        }
    }
}
