﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace ImpErpConsole
{
    public class HubSpotDeals
    {
        public string PortalId { get; set; }
        public string DealId { get; set; }
        public Dictionary<string, string> Properties { get; set; }
       
    }
    public class HubSpotClient
    {
        private HttpClient httpClient;
        private IConfiguration _config;
        public HubSpotClient(IConfiguration config )
        {
            _config = config;
            httpClient = new HttpClient();

        }

        public void GetWonDeals()
        {
            var dt = DateTime.UtcNow;
            var dateTimeOffset = new DateTimeOffset(dt);
            var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();
            ///deals/v1/deal/paged
             using var taskRc = httpClient.GetAsync("https://api.hubapi.com/deals/v1/deal/recent/created?hapikey=315990d6-5362-4034-b029-fe8cbc081ee1");

            
            var respRc = taskRc.Result;

            var resultRc = respRc.Content.ReadAsStringAsync().Result;



            //var uriWithKey = "https://api.hubapi.com/deals/v1/deal/paged?hapikey=315990d6-5362-4034-b029-fe8cbc081ee1";
            //var propertyList = "&properties=dealstage&properties=amount_in_home_currency&properties=dealtype&properties=closedate" +
            //                    "&properties=lead_source&properties=createdate&properties=dealname&properties=hubspot_owner_id";
            //var requestUri = uriWithKey + propertyList;

            //using var task = httpClient.GetAsync(requestUri);
            //var resp = task.Result;

            var result = resp.Content.ReadAsStringAsync().Result;


            JObject jObj = JObject.Parse(result);
            JArray test = (JArray)jObj.SelectToken("deals");
            List<HubSpotDeals> dealWon = new List<HubSpotDeals>();

            foreach (JObject item in test)
            {
                var property = item.GetValue("properties");

                var createDate = property["createdate"]?.SelectToken("value").ToString();

                if (createDate != null)
                {
                    var crDate = createDate.Substring(0, createDate.Length - 3);
                    System.DateTime crDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    _ = ulong.TryParse(crDate, out ulong cdate);
                    var dealCreateDate = crDateTime.AddSeconds(cdate).ToUniversalTime();

                    var newDealDate = DateTime.UtcNow.AddDays(-1);

                    if (dealCreateDate > newDealDate)
                    {
                        var deal = new HubSpotDeals
                        {
                            DealId = item.GetValue("dealId").ToString(),
                            PortalId = item.GetValue("portalId").ToString(),
                            Properties = new Dictionary<string, string>()

                        };
                        var amount = property["amount_in_home_currency"]?.SelectToken("value").ToString();
                        var dealtype = property["dealtype"]?.SelectToken("value").ToString();
                        var source = property["lead_source"]?.SelectToken("value").ToString();
                        
                        var closedDate = property["createdate"]?.SelectToken("value").ToString();
                        var dealname = property["dealname"]?.SelectToken("value").ToString();
                        var dealowner = property["hubspot_owner_id"]?.SelectToken("value").ToString();
                        var crdateStr = dealCreateDate.ToString();

                        if (closedDate != null)
                        {
                            var clDate = closedDate.Substring(0, closedDate.Length - 3);
                            System.DateTime clDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                            _ = ulong.TryParse(clDate, out ulong closedate);
                            closedDate = clDateTime.AddSeconds(closedate).ToUniversalTime().ToString();
                        }

                        var prop = new Dictionary<string, string>
                            {
                                {"DealName",dealname },
                                {"DealStage","Closedwon" },
                                {"DealType",dealtype },
                                {"Amount",amount },
                                {"LeadSource",source },
                                {"CreatedDate",crdateStr },
                                {"ClosedDate",closedDate },
                            };
                        deal.Properties = prop;
                        dealWon.Add(deal);

                    }
                    else
                    {
                        continue;
                    }
                }

               
            }

            InsertDeals(dealWon);

            Console.WriteLine("Deal Generated");
        }

        public void InsertDeals(List<HubSpotDeals> hubspotDealList)
        {
            string cs = _config["connstr"];

            MySqlConnection jogetConn = null;


            #region JoGet Table Entry
            string jogetConnStr = @"server=localhost;database=jwdb;user=root;password=mySqlPass123$";


            try
            {
                jogetConn = new MySqlConnection(jogetConnStr);
                jogetConn.Open();

                MySqlCommand comm = jogetConn.CreateCommand();

                //-Updating duplicate record
                /*
                        INSERT INTO BLOGPOSTs ( postId, postTitle, postPublished)
                        VALUES(5, 'Python Tutorial', '2019-08-04')
                        ON DUPLICATE KEY UPDATE
                            postId = 5, postTitle = 'Python Tutorial', postPublished = '2019-08-04';
                */

                comm.CommandText = "INSERT INTO app_fd_staffing_plan(hubspotdealid,dealCreated,createdBy,c_status,c_currency) " +
                                   "VALUES(?hubspotdealid,?dealCreated, ?createdBy,?c_status,?c_currency)" ;

                foreach (var deal in hubspotDealList)
                {
                    comm.Parameters.AddWithValue("?dealCreated", deal.Properties["CreatedDate"]);
                    comm.Parameters.AddWithValue("?createdBy", deal.Properties["LeadSource"]);
                    comm.Parameters.AddWithValue("?c_status", deal.Properties["DealStage"]);
                    comm.Parameters.AddWithValue("?c_currency", deal.Properties["Amount"]);                   

                    // comm.CommandText =$"INSERT INTO HubSpotDeals(PortalId,DealId,DealName,DealStage,Amount,LeadSource,CreatedDate,ClosedDate) VALUES({deal.PortalId}, {deal.DealId},{deal.Properties["DealName"]},{deal.Properties["DealStage"]},{deal.Properties["Amount"]},{deal.Properties["LeadSource"]},{deal.Properties["CreatedDate"]},{deal.Properties["ClosedDate"]})";

                    comm.ExecuteNonQuery();

                    comm.Parameters.Clear();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (jogetConn != null)
                {
                    jogetConn.Close();
                }

            }

            #endregion JoGet Table Entry


            // Now Commented 
            #region HubSpotDeals DB Entry
            /*
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                MySqlCommand comm = conn.CreateCommand();
                comm.CommandText = "INSERT INTO HubSpotDeals(PortalId,DealId,DealName,DealStage,Amount,LeadSource,CreatedDate,ClosedDate) " +
                                   "VALUES(?PortalId, ?DealId,?DealName,?DealStage,?Amount,?LeadSource,?CreatedDate, ?ClosedDate)";
                
                foreach (var deal in hubspotDealList)
                {
                    comm.Parameters.AddWithValue("?PortalId", deal.PortalId);
                    comm.Parameters.AddWithValue("?DealId", deal.DealId);
                    comm.Parameters.AddWithValue("?DealName", deal.Properties["DealName"]);
                    comm.Parameters.AddWithValue("?DealStage", deal.Properties["DealStage"]);
                    comm.Parameters.AddWithValue("?Amount", deal.Properties["Amount"]);
                    comm.Parameters.AddWithValue("?LeadSource", deal.Properties["LeadSource"]);
                    comm.Parameters.AddWithValue("?CreatedDate", deal.Properties["CreatedDate"]);
                    comm.Parameters.AddWithValue("?ClosedDate", deal.Properties["ClosedDate"]);

                    // comm.CommandText =$"INSERT INTO HubSpotDeals(PortalId,DealId,DealName,DealStage,Amount,LeadSource,CreatedDate,ClosedDate) VALUES({deal.PortalId}, {deal.DealId},{deal.Properties["DealName"]},{deal.Properties["DealStage"]},{deal.Properties["Amount"]},{deal.Properties["LeadSource"]},{deal.Properties["CreatedDate"]},{deal.Properties["ClosedDate"]})";
                    
                    comm.ExecuteNonQuery();

                    comm.Parameters.Clear();
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (conn != null)
                {
                    conn.Close();
                }

            }
             */

            #endregion HubSpotDeals DB Entry


        }
    }
}
