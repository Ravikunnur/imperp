using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;

namespace HubspotDemo
{
    public class Startup
    {
        private readonly string corsName = "localhost";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddHttpClient();

            services.AddCors(options =>
            {
                options.AddPolicy(corsName,
                builder =>
                {
                    builder.WithOrigins("http://localhost");
                });
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = "Hubspot";
            })
                .AddCookie()
                .AddOAuth("Hubspot", options =>
                {
                    options.ClientId = Configuration["Hubspot:ClientId"];
                    options.ClientSecret = Configuration["Hubspot:ClientSecret"];
                    options.CallbackPath = new PathString("/Home");

                    options.AuthorizationEndpoint = Configuration["Hubspot:OauthUrl"];
                    options.TokenEndpoint = Configuration["Hubspot:TokenEndpoint"];
                    options.Scope.Clear();
                    options.Scope.Add(Configuration["Hubspot:Scope"]);
                    options.SaveTokens = true;
                    //options.UserInformationEndpoint = "https://api.github.com/user";

                    //                    options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
                    //                    options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                    //                    options.ClaimActions.MapJsonKey("urn:github:login", "login");
                    //                    options.ClaimActions.MapJsonKey("urn:github:url", "html_url");
                    //                    options.ClaimActions.MapJsonKey("urn:github:avatar", "avatar_url");

                    //                    options.Events = new OAuthEvents
                    //                    {
                    //                        OnCreatingTicket = async context =>
                    //                        {
                    //                            var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
                    //                            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //                            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);

                    //#pragma warning disable CA2007 // Consider calling ConfigureAwait on the awaited task
                    //                            var response = await context.Backchannel.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, context.HttpContext.RequestAborted);
                    //#pragma warning restore CA2007 // Consider calling ConfigureAwait on the awaited task
                    //                            response.EnsureSuccessStatusCode();

                    //#pragma warning disable CA2007 // Consider calling ConfigureAwait on the awaited task
                    //                            var user = JObject.Parse(await response.Content.ReadAsStringAsync());
                    //#pragma warning restore CA2007 // Consider calling ConfigureAwait on the awaited task

                    //                            context.RunClaimActions(user);
                    //                        }
                    //                    };
                }
                )
                
                ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseCors(corsName);
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
