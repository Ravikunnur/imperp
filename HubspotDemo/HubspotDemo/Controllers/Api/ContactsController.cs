﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Helpers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HubspotDemo.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly string _baseUrl = "https://api.hubapi.com";
        private readonly IHttpClientFactory _clientFactory;

        public ContactsController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        [HttpGet]
        [Route("getdeal")]
        public async Task<ActionResult<string>> GetDeal(int dealId = 1)
        {
            var url = _baseUrl + " /deals/v1/deal/:"+dealId;
            var accessToken = await HttpContext.GetTokenAsync("access_token");


            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return "Failure";
        }

        //// GET: api/Contacts
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/Contacts/5
        [HttpGet("{count}", Name = "Get")]
        public async Task<ActionResult<string>> GetAsync(int count = 1)
        {
            //var url = _baseUrl + "/contacts/v1/lists/all/contacts/all?count=" + count;

            var url = _baseUrl + " /deals/v1/deal/:" + count;
            var accessToken = await HttpContext.GetTokenAsync("access_token");


            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            return "Failure";
        }

        // POST: api/Contacts
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Contacts/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
