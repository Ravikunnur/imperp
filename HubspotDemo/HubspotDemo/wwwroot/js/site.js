﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$('#getContacts').click(() => {
    const count = $('#count').val();

    if (count != undefined && count != null) {
        const url = "http://localhost:56226/api/contacts/" + count;
        $.get(url, (data, status) => {

            console.log(data);
            result = JSON.parse(data);
            if (result != null) {
                const contactElement = $('#contacts');
                contactElement.empty();
                result.contacts.forEach(contact => {

                    let card = '<div class="col col-lg-4 m-2">' +
                        '<div class="card" style="width:200px">' +
                        '<img class="card-img-top" src="https://www.w3schools.com/bootstrap4/img_avatar1.png" alt="Card image" style="width:100%">' +
                        ' <div class="card-body"> <h4 class="card-title">' +
                        contact.properties.firstname.value + ' ' + contact.properties.lastname.value +
                        '</h4> <p class="card-text">' + contact.properties.company.value +
                        '</p> <a href="' + contact["profile-url"] +
                        '" target="blank" class="btn btn-primary">See Profile</a></div></div></div>';
                    contactElement.append(card);
                });
            }
            console.log(result, status);
        });
    }
});