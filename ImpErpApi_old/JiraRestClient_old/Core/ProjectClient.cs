using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using ImpErpApi.Domain;
using ImpErpApi.Util;

namespace ImpErpApi.Core
{
    /// <summary>
    /// Client to get Issues
    /// </summary>
    public class ProjectClient : BaseClient
    {
        private ResultModel result;
        public ProjectClient(JiraRestClient jiraRestClient) : base(jiraRestClient)
        {
            result = new ResultModel();
        }
        public async Task<ResultModel> CreateProject(StringContent createIssueJson)
        {
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.PROJECT);
            // createIssueJson.Headers.Add("Content-Type", "application/json");
            var completeUri = restUriBuilder.ToString();
            var resp = await client.PostAsync(completeUri, createIssueJson);
            result.Data = resp.Content.ReadAsStringAsync();
            result.IsSuccess = true;

            return result;
        }
        
        public async Task<Project> GetProjectByKey(string key)
        {
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.PROJECT);
            restUriBuilder.Query = RestParamConstants.PROJECT + "=" + key;
            var completeURI = restUriBuilder.ToString();
            var stream = client.GetStreamAsync(completeURI);
            var serializer = new DataContractJsonSerializer(typeof(Project));
            return serializer.ReadObject(await stream) as Project;
        }

        public async Task<List<Project>> GetAllProjects()
        {
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.PROJECT);
            var stream = client.GetStreamAsync(restUriBuilder.ToString());

            
            var serializer = new DataContractJsonSerializer(typeof(List<Project>));
            return serializer.ReadObject(await stream) as List<Project>;
        }

        public async Task<List<Version>> GetProjectVersions(string key){
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.PROJECT, key, RestPathConstants.VERSIONS);
            var stream = client.GetStreamAsync(restUriBuilder.ToString());
            var serializer = new DataContractJsonSerializer(typeof(List<Version>));
            return serializer.ReadObject(await stream) as List<Version>;
        }

        public async Task<List<Component>> GetProjectComponents(string key){
            var restUriBuilder = UriHelper.BuildPath(baseUri, RestPathConstants.PROJECT, key, RestPathConstants.COMPONENTS);
            var stream = client.GetStreamAsync(restUriBuilder.ToString());
            
            var serializer = new DataContractJsonSerializer(typeof(List<Component>));
            return serializer.ReadObject(await stream) as List<Component>;
        }
    }
}