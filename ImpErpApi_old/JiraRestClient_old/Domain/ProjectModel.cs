﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImpErpApi.Domain
{
    public class ProjectModel
    {
        public string ProjectName { get; set; }
        public string Key { get; set; }
        public long Id { get; set; }

    }
}
