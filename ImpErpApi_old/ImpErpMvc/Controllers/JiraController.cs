﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ImpErpMvc.Models;
using Newtonsoft.Json;
using ImpErpMvc.Service;
using ImpErpCommon.Models;

namespace ImpErpMvc.Controllers
{
    public class JiraController : Controller
    {
        private ResultModel result;
        private ApiClient _apiClient;

        public JiraController()
        {
            _apiClient = new ApiClient();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetAllProjects()
        {
            return View("GetAllProjects");
        }
        public JsonResult GetProjects()
        {
            var response = _apiClient.GetAllProject();
           
            var jsonList = JsonConvert.DeserializeObject<List<ProjectModel>>(response);

            var jsonResp = JsonConvert.SerializeObject(jsonList);

            return Json(jsonResp);            
            
        }
    }
}