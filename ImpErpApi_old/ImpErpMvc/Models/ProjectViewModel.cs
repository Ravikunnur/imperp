﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImpErpCommonModel.Models;

namespace ImpErpMvc.Models
{
    public class ProjectViewModel
    {
        public string ProjectName { get; set; }
        public string Key { get; set; }
        public long Id { get; set; }
    }
}
