﻿using ImpErpApi.Util;
using ImpErpCommon.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ImpErpApi
{
    public class JiraTempoClient
    {
        private readonly HttpClient _client;
        private readonly Uri _baseUri;
        private ResultModel result;

        public JiraTempoClient(Uri uri, string apiToken)
        {
            result = new ResultModel();
            this._client = new HttpClient();
            this._baseUri = uri;
            // Balaji Tempo Token
            apiToken = "vbdLgIR0Q06osHeNNbxG6tKgj0r3Q3";
            _client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + apiToken);
        }


        public ResultModel GetAllWorkLogs()
        {
            var restUriBuilder = UriHelper.BuildTempoPath(this._baseUri, RestPathConstants.TEMPOWORKLOGS);
            var completeUri = restUriBuilder.ToString();
            var fromDate = "from=2019-11-10";
            var toDate = "to=2019-11-16";
            //https://api.tempo.io:443/core/3/worklogs?from=2019-11-10&to=2019-11-19
            using var task =  _client.GetAsync(completeUri);
           

            var resp = task.Result;
            result.Data = resp.Content.ReadAsStringAsync();

            var r1 = resp.Content.ReadAsStringAsync().Result;

            JObject jObj = JObject.Parse(r1);
            JArray test = (JArray)jObj.SelectToken("results");
            List<TempoWorkLogs> worklogs = new List<TempoWorkLogs>();

            foreach(JObject item in test)
            {
                var property = item.GetValue("issue");

                var IssueKey = property["key"].ToString(); 
                var IssueId = property["id"].ToString();
                var WorkLogId = item.GetValue("tempoWorklogId").ToString();

                var timeSpentStr = item.GetValue("timeSpentSeconds").ToString();
                _ = long.TryParse(timeSpentStr, out long timeHrs);
                var TimeSpentInHrs = (int) timeHrs / (60 * 60 ) ;

                var BillableHrsStr = item.GetValue("billableSeconds").ToString();
                _ = long.TryParse(BillableHrsStr, out long billHrs);
                var BillableHrs = (int) billHrs /( 60 * 60);

                var StartDate = item.GetValue("startDate").ToString();
                var StartTime = item.GetValue("startTime").ToString();
                var CreatedAt = item.GetValue("createdAt").ToString();
                var UpdatedAt = item.GetValue("updatedAt").ToString();

                var IssueDesc = item.GetValue("description").ToString();
                property = item.GetValue("author");

                var DisplayName = property["displayName"].ToString();
                var AccountId = property["accountId"].ToString();

                var projectKey = IssueKey.Substring(0, IssueKey.IndexOf('-'));
                TempoWorkLogs worklog = new TempoWorkLogs
                {
                    IssueId = IssueId,
                    IssueKey = IssueKey,
                    ProjectKey = projectKey,
                    ProjectName = "TODO:",
                    WorkLogId = WorkLogId,
                    IssueDesc = IssueDesc,
                    BillableHrs = BillableHrs,
                    TimeSpentInHrs = TimeSpentInHrs,
                    StartDate = StartDate,
                    StartTime = StartTime,
                    DisplayName = DisplayName,
                    CreatedAt = CreatedAt,
                    UpdatedAt = UpdatedAt,
                    UserAccountId = AccountId
                };
                worklogs.Add(worklog);
            }

            
            return result;
        }

     
        public async Task<ResultModel> CreateWorkLog(StringContent createWorkLog)
        {
            var restUriBuilder = UriHelper.BuildTempoPath(_baseUri, RestPathConstants.TEMPOWORKLOGS);
            // createIssueJson.Headers.Add("Content-Type", "application/json");
            var completeUri = restUriBuilder.ToString();
            var resp = await _client.PostAsync(completeUri, createWorkLog);
            result.Data = resp.Content.ReadAsStringAsync();
            result.IsSuccess = true;
            return result;
        }
    }
}
