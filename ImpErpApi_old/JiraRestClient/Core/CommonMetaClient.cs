﻿using System.Threading.Tasks;
using ImpErpApi.Domain;
using ImpErpApi.Util;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ImpErpApi.Core
{
    public class CommonMetaClient : BaseClient
    {
        public CommonMetaClient(JiraRestClient jiraRestClient) : base(jiraRestClient)
        {

        }
    }
}
