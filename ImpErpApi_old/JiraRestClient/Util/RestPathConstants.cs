﻿namespace ImpErpApi.Util
{
    public class RestPathConstants : JsonConstants
    {
        // REST Paths
        public const string BASE_REST_PATH = "rest/api/2";

        public const string PROJECT = "project";

        public const string USER = "user";

        public const string SEARCH = "search";

        public const string ISSUE = "issue";

        public const string COMMENT = "comment";

        public const string VERSIONS = "versions";

        public const string COMPONENTS = "components";

        public const string ISSUETPYES = "issuetype";

        public const string STATUS = "status";

        public const string PRIORITY = "priority";

        public const string TRANSITIONS = "transitions";

        public const string WORKLOG = "worklog";

        public const string ATTACHMENTS = "attachments";

        public const string ASSIGNABLE = "assignable";

        public const string SERVER_INFO = "serverInfo";

        public const string CREATEMETA = "createmeta";


        public const string TEMPOWORKLOGS = "worklogs";

        public const string MYSELF = "myself";

        public const string HSP_DEALS = "/deals";
        public const string HSP_VERSIONDEAL = "/v1/deal";
        public const string HSP_RECENTCREATED = "/recent/created";
    }
}
