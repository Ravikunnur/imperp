﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ImpErpApi.Core;
using ImpErpApi.Domain;
using ImpErpApi.Util;
using ImpErpCommon.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using ImpErpCommon.DBConnect;
using MySql.Data.MySqlClient;

namespace ImpErpApi
{
     public class HubSpotClient
    {
        private HttpClient httpClient;
        private IOptions<HubSpotDetails> _hubSpotDetails;
        private ResultModel resultModel;
        private Dictionary<string, string> authenticationCredentials;
        private readonly string _baseUri;
        private string _apiToken;

        private ResultModel result;

        public HubSpotClient(string uri, string apiToken)
        {
            resultModel = new ResultModel();
            this.httpClient = new HttpClient();
            this._baseUri = uri;
            this._apiToken = apiToken;
           // httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + apiToken);
        }

        public async Task<ResultModel> GetDealById(int dealId =1)
        {
            // /deals/v1/deal/:dealId   https://api.hubapi.com       

           // using var task = httpClient.GetAsync("https://api.hubapi.com/deals/v1/deal/462527573?hapikey=" + _apiToken);

            var UriPath = UriHelper.BuildHubSpotUri(this._baseUri, RestPathConstants.HSP_DEALS, RestPathConstants.HSP_VERSIONDEAL);

            dealId = 1213771156; // 1117974552; // 462527573;
            var requestUri = UriPath + "/" + dealId.ToString();
            var uriWithKey = requestUri + "?hapikey=" + _apiToken;
            using var task = httpClient.GetAsync(uriWithKey);
            var resp = task.Result;
            resultModel.Data = resp.Content.ReadAsStringAsync(); 
            return resultModel;
        }

        public async Task<ResultModel> GetPreviousdayDeal()
        {
            var dt = DateTime.UtcNow;
            var dateTimeOffset = new DateTimeOffset(dt);
            var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();               

            // using var task = httpClient.GetAsync("https://api.hubapi.com/deals/v1/deal/recent/created?hapikey=" + _apiToken);

            var UriPath = UriHelper.BuildHubSpotUri(this._baseUri, RestPathConstants.HSP_DEALS, RestPathConstants.HSP_VERSIONDEAL);

            var urirecentCreated = UriPath + RestPathConstants.HSP_RECENTCREATED; // "/recent/created";
           
            var uriWithKey = urirecentCreated + "?hapikey=" + _apiToken;
            var requestUri = uriWithKey + "&since=" + unixDateTime.ToString();

            using var task = httpClient.GetAsync(requestUri);
            var resp = task.Result;
            var result =  resp.Content.ReadAsStringAsync().Result;
            resultModel.Data = result;

            #region Custom Model
            /*  JObject jObj = JObject.Parse(resultModel.Data);
              JArray test = (JArray)jObj.SelectToken("results");
              List<HubSpotDeals> dealWon = new List<HubSpotDeals>();

              foreach (JObject item in test)
              {
                  var property = item.GetValue("properties");

                      var deal = new HubSpotDeals
                      {
                          DealId = item.GetValue("dealId").ToString(),
                          PortalId = item.GetValue("portalId").ToString(),
                          Properties = new List<HubSpotProperty>()

                      };

                      var prop = new HubSpotProperty
                      {
                          Name = "DealStage",
                          Value = "Closedwon"
                      };

                      deal.Properties.Add(prop);
                      dealWon.Add(deal);

              }

              var jstring = System.Text.Json.JsonSerializer.Serialize(dealWon);

              resultModel.Data = jstring; */

            #endregion Custom Model

            return resultModel;
        }

        //dealstage - closedwon

            public async Task<ResultModel> GetWonDeals()
            {
                var dt = DateTime.UtcNow;
                var dateTimeOffset = new DateTimeOffset(dt);
                var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();
                ///deals/v1/deal/paged
                // using var task = httpClient.GetAsync("https://api.hubapi.com/deals/v1/deal/recent/created?hapikey=" + _apiToken);

                var UriPath = UriHelper.BuildHubSpotUri(this._baseUri, RestPathConstants.HSP_DEALS, RestPathConstants.HSP_VERSIONDEAL);

                var urirecentCreated = UriPath + "/paged"; // "/recent/created";

                var uriWithKey = urirecentCreated + "?hapikey=" + _apiToken;
                var propertyList = "&properties=dealstage&properties=amount_in_home_currency&properties=dealtype&properties=closedate" +
                    "               &properties=lead_source&properties=createdate&properties=dealname&properties=hubspot_owner_id";
                var requestUri = uriWithKey + propertyList;

                using var task = httpClient.GetAsync(requestUri);
                var resp = task.Result;

                var result = resp.Content.ReadAsStringAsync().Result;
         

                JObject jObj = JObject.Parse(result);
                JArray test = (JArray)jObj.SelectToken("deals");
                List<HubSpotDeals> dealWon = new List<HubSpotDeals>();

                foreach (JObject item in test)
                {
                    var property = item.GetValue("properties");

                var createDate1 = property["createdate"]?.SelectToken("value").ToString();

                if(createDate1 !=null)
                {
                    var crDate = createDate1.Substring(0, createDate1.Length - 3);
                    System.DateTime crDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    _ = ulong.TryParse(crDate, out ulong cdate);
                    var dealCreateDate = crDateTime.AddSeconds(cdate).ToUniversalTime();

                    var newDealDate = DateTime.UtcNow.AddDays(-1);

                    if (dealCreateDate > newDealDate)
                    {

                    }
                    else
                    {
                        continue;
                    }
                }
                if (property["dealstage"].SelectToken("value").ToString() == "closedwon")
                    {
                        var deal = new HubSpotDeals
                        {
                            DealId = item.GetValue("dealId").ToString(),
                            PortalId = item.GetValue("portalId").ToString(),
                            Properties = new Dictionary<string, string>()

                        };
                        var amount = property["amount_in_home_currency"]?.SelectToken("value").ToString();
                        var dealtype = property["dealtype"]?.SelectToken("value").ToString();
                        var source = property["lead_source"]?.SelectToken("value").ToString();
                        var createDate = property["createdate"]?.SelectToken("value").ToString();
                        var closedDate = property["createdate"]?.SelectToken("value").ToString();
                        var dealname = property["dealname"]?.SelectToken("value").ToString();
                        var dealowner = property["hubspot_owner_id"]?.SelectToken("value").ToString();

                   
                        //if (createDate != null)
                        //{
                        //    System.DateTime crDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                        //    _ = Double.TryParse(createDate, out double cdate);
                        //    createDate = crDateTime.AddSeconds(cdate).ToUniversalTime().ToString();
                        //}

                        //if (closedDate != null)
                        //{
                        //    System.DateTime clDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                        //    _ = Double.TryParse(createDate, out double cdate);
                        //    closedDate = clDateTime.AddSeconds(cdate).ToUniversalTime().ToString();
                        //}

                        var prop = new Dictionary<string, string>
                        {
                            {"DealName",dealname },
                            {"DealStage","Closedwon" },
                            {"DealType",dealtype },
                            {"Amount",amount },
                            {"LeadSource",source },                       
                            {"CreatedDate",createDate },
                            {"ClosedDate",closedDate },

                        };


                        deal.Properties = prop;
                        dealWon.Add(deal);
                    }
                }

                var jstring = System.Text.Json.JsonSerializer.Serialize(dealWon);

                resultModel.Data = jstring;
                return resultModel;        
            }

        #region Support Methods
        private void AddDealsInDB()
        {
            MySqlConnection conn = new MySqlConnection();
            using var cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO `HubSpotDeals` (`Title`, `Content`) VALUES (@title, @content);";
           
            var resp = cmd.ExecuteNonQueryAsync().Result;
            var Id = (int)cmd.LastInsertedId;


        }
        #endregion Support Methods


    }
}
