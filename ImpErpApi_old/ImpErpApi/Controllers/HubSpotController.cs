﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ImpErpCommon.Models;
using Microsoft.AspNetCore.Authentication;

namespace ImpErpApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HubSpotController : ControllerBase
    {
        private readonly string _baseUrl = "https://api.hubapi.com";
        private readonly IHttpClientFactory _clientFactory;
        private ResultModel resultModel;
        public HubSpotController(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            resultModel = new ResultModel();
        }

        // GET: api/Contacts/5
        [HttpGet("{count}", Name = "Get")]
        public async Task<ResultModel> GetAsync(int count = 1)
        {
            var url = _baseUrl + "/contacts/v1/lists/all/contacts/all?count=" + count;
            var accessToken = await HttpContext.GetTokenAsync("access_token");


            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "Bearer " + accessToken);

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var resp=  await response.Content.ReadAsStringAsync();
                resultModel.Data = resp;
            }

            resultModel.Message = "Failure";
            return resultModel;
        }
    }
}