﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ImpErpApi.Domain;
using ImpErpCommon.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace ImpErpApi.Controllers
{
    public class JiraTempoController : Controller
    {
        private JiraTempoClient restClient;
        private IOptions<JiraTempoDetails> _jiraDetails;
        private ResultModel resultModel;
        public JiraTempoController(IOptions<JiraTempoDetails> jiraDetails)
        {
            _jiraDetails = jiraDetails;
            Uri url = new Uri(_jiraDetails.Value.JiraTempoUrl);          
            var password = _jiraDetails.Value.JiraTempoPassword;  //_configurationRoot["JiraPassword"];
            restClient = new JiraTempoClient(url, password);
            resultModel = new ResultModel();            
        }

        [HttpGet]
        [Route("getallworklogs")]
        public ResultModel GetAllWorkLogs()
        {
            resultModel = restClient.GetAllWorkLogs();
            
            return resultModel;
        }

        [HttpPost]
        [Route("logwork")]
        public ResultModel LogWork()
        {
            var post_params = @"{
                                  ""issueKey"": ""IE-14"",
                                  ""timeSpentSeconds"": 3600,                                 
                                  ""startDate"": ""2019-11-27"",
                                  ""startTime"": ""20:06:00"",
                                  ""description"": ""Integrating Tempo using tempo api -TEST"",
                                  ""authorAccountId"": ""557058:4c974ba7-bc46-4c73-b600-cfc738633b4c"",
                                  ""remainingEstimateSeconds"": 7200,
                                  ""attributes"": [  ]
                                 }";
            var content = new StringContent(post_params, Encoding.UTF8, "application/json");
            var resp = restClient.CreateWorkLog(content);
            var result = resp.GetAwaiter().GetResult();
            resultModel.Data = result;           
            return resultModel;
        }

        [HttpPost]
        [Route("webhook")]
        public ResultModel HubSpotWebhook([FromBody] HSPayLoad hsPayload)
        {
            LogWork();
            return resultModel;
        }
    }
}