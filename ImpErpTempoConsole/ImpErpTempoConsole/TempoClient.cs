﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ImpErpTempoConsole
{
    #region Models
    public class ProjectModel
    {
        public string ProjectName { get; set; }
        public string Key { get; set; }
        public string Id { get; set; }
    }

    public class TempoWorkLogs
    {
        public string WorkLogId { get; set; }
        public string IssueId { get; set; }
        public string IssueKey { get; set; }
        public string IssueDesc { get; set; }
        public string ProjectKey { get; set; }
        public string ProjectName { get; set; }
        public string UserAccountId { get; set; }
        public string DisplayName { get; set; }


        public int TimeSpentInHrs { get; set; }
        public int BillableHrs { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string CreatedAt { get; set; }
        public string UpdatedAt { get; set; }

    }
    #endregion

    public class TempoClient
    {
        private readonly HttpClient _client;

        public TempoClient()
        {   
            this._client = new HttpClient(); 
        }

        public async void GetAllJiraProjects()
        {
           
            string jiraUri = "https://impiger.atlassian.net/rest/api/2/project?expand=lead";

            //   "JiraUsername": "ravindran.ramachandran@impigertech.com",
            //   "JiraPassword": "tkB6azKzEelXBFqSN8WZD6B4",

            string username = "ravindran.ramachandran@impigertech.com";
            string password = "tkB6azKzEelXBFqSN8WZD6B4";
            var bytes = Encoding.ASCII.GetBytes(username + ":" + password);
            var token = Convert.ToBase64String(bytes);
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
                        
            var resp = _client.GetStringAsync(jiraUri).Result;
           
            resp = "{ \"result\":" + resp + "}";
            JObject jObj = JObject.Parse(resp);
            JArray test = (JArray)jObj.SelectToken("result");
            List<ProjectModel> projects = new List<ProjectModel>();

            foreach (JObject item in test)
            {
                var key = item.GetValue("key").ToString();
                var projectName = item.GetValue("name").ToString();
                var Id = item.GetValue("id").ToString();
                var leadProp = item.GetValue("lead");

                var lead = leadProp["displayName"].ToString();

                ProjectModel project = new ProjectModel
                {
                   ProjectName = projectName,
                   Key = key,
                   Id = Id
                };
                projects.Add(project);
            }
            InsertProjects(projects);
        }

        public void InsertProjects(List<ProjectModel> projects)
        {
            //string cs = _config["connstr"];

            MySqlConnection jogetConn = null;

            #region JoGet Table Entry
            string jogetConnStr = @"server=localhost;database=jwdb;user=root;password=mySqlPass123$";

            try
            {
                jogetConn = new MySqlConnection(jogetConnStr);
                jogetConn.Open();

                MySqlCommand comm = jogetConn.CreateCommand();

                //-Updating duplicate record
                /*
                        INSERT INTO BLOGPOSTs ( postId, postTitle, postPublished)
                        VALUES(5, 'Python Tutorial', '2019-08-04')
                        ON DUPLICATE KEY UPDATE
                            postId = 5, postTitle = 'Python Tutorial', postPublished = '2019-08-04';
                */

                comm.CommandText = "INSERT INTO app_fd_worklogs(c_issueSummery,c_hours,c_issueKey) " +
                                   "VALUES(?c_issueSummery,?c_hours, ?c_issueKey,)";

                int i = 0;

                foreach (var prj in projects)
                {

                    comm.Parameters.AddWithValue("?c_issueSummery", prj.Id);
                    comm.Parameters.AddWithValue("?c_hours", prj.Key);
                    comm.Parameters.AddWithValue("?c_issueKey", prj.ProjectName);


                    // comm.CommandText =$"INSERT INTO HubSpotDeals(PortalId,DealId,DealName,DealStage,Amount,LeadSource,CreatedDate,ClosedDate) VALUES({deal.PortalId}, {deal.DealId},{deal.Properties["DealName"]},{deal.Properties["DealStage"]},{deal.Properties["Amount"]},{deal.Properties["LeadSource"]},{deal.Properties["CreatedDate"]},{deal.Properties["ClosedDate"]})";

                    comm.ExecuteNonQuery();

                    comm.Parameters.Clear();

                    if (i == 0)
                        break;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (jogetConn != null)
                {
                    jogetConn.Close();
                }

            }

            #endregion JoGet Table Entry


        }

        public void GetUpdatedLogs()
        {
            string apiToken = "vbdLgIR0Q06osHeNNbxG6tKgj0r3Q3";
            _client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + apiToken);

            // URI REF  https://api.tempo.io:443/core/3/worklogs?from=2019-11-10&to=2019-11-19&limit=1000


            var completeUri = "https://api.tempo.io:443/core/3/worklogs?updatedFrom=2019-12-01";

            using var resp = _client.GetAsync(completeUri).Result;

            var resultStr = resp.Content.ReadAsStringAsync().Result;

            JObject jObj = JObject.Parse(resultStr);
            JArray test = (JArray)jObj.SelectToken("results");


           
            List<TempoWorkLogs> worklogs = new List<TempoWorkLogs>();
        }
        public void GetAllWorkLogs()
        {
            // Balaji Tempo Token
            string apiToken = "vbdLgIR0Q06osHeNNbxG6tKgj0r3Q3";
            _client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + apiToken);

            // URI REF  https://api.tempo.io:443/core/3/worklogs?from=2019-11-10&to=2019-11-19&limit=1000

            

            var novDt = new DateTime(2019, 11, 01);
            var tDays = DateTime.DaysInMonth(2019, 11);

            var lastCallDays = tDays % 7;
            List<TempoWorkLogs> worklogs = new List<TempoWorkLogs>();

            for (int i=0;i<5;i++)
            {
                var completeUri = "https://api.tempo.io:443/core/3/worklogs?from=FRDT&to=TODT&limit=1000";

                #region GetDateRange
                string frdt = string.Empty;
                string todt = string.Empty;

                if (i==4)
                {
                    frdt = novDt.ToString("yyyy-MM-dd");
                    novDt = novDt.AddDays(lastCallDays-1);
                    todt = novDt.ToString("yyyy-MM-dd");
                }
                else
                {
                    frdt = novDt.ToString("yyyy-MM-dd");
                    novDt = novDt.AddDays(6);
                    todt = novDt.ToString("yyyy-MM-dd");
                }

                completeUri = completeUri.Replace("FRDT", frdt).Replace("TODT", todt);
                novDt = novDt.AddDays(1);

                #endregion GetDataRange

                #region Fetch'n'Fill
                using var resp = _client.GetAsync(completeUri).Result;

                var resultStr = resp.Content.ReadAsStringAsync().Result;

                JObject jObj = JObject.Parse(resultStr);
                JArray test = (JArray)jObj.SelectToken("results");
               

                foreach (JObject item in test)
                {
                    var property = item.GetValue("issue");

                    var IssueKey = property["key"].ToString();
                    var IssueId = property["id"].ToString();
                    var WorkLogId = item.GetValue("tempoWorklogId").ToString();

                    var timeSpentStr = item.GetValue("timeSpentSeconds").ToString();
                    _ = long.TryParse(timeSpentStr, out long timeHrs);
                    var TimeSpentInHrs = (int)timeHrs / (60 * 60);

                    var BillableHrsStr = item.GetValue("billableSeconds").ToString();
                    _ = long.TryParse(BillableHrsStr, out long billHrs);
                    var BillableHrs = (int)billHrs / (60 * 60);

                    var StartDate = item.GetValue("startDate").ToString();
                    var StartTime = item.GetValue("startTime").ToString();
                    var CreatedAt = item.GetValue("createdAt").ToString();
                    var UpdatedAt = item.GetValue("updatedAt").ToString();

                    var IssueDesc = item.GetValue("description").ToString();
                    property = item.GetValue("author");

                    var DisplayName = property["displayName"].ToString();
                    var AccountId = property["accountId"].ToString();

                    var projectKey = IssueKey.Substring(0, IssueKey.IndexOf('-'));
                    TempoWorkLogs worklog = new TempoWorkLogs
                    {
                        IssueId = IssueId,
                        IssueKey = IssueKey,
                        ProjectKey = projectKey,
                        ProjectName = "TODO:",
                        WorkLogId = WorkLogId,
                        IssueDesc = IssueDesc,
                        BillableHrs = BillableHrs,
                        TimeSpentInHrs = TimeSpentInHrs,
                        StartDate = StartDate,
                        StartTime = StartTime,
                        DisplayName = DisplayName,
                        CreatedAt = CreatedAt,
                        UpdatedAt = UpdatedAt,
                        UserAccountId = AccountId
                    };
                    worklogs.Add(worklog);
                }
               
                #endregion Fetch'n'Fill
            }

            //InsertWorkLog(worklogs);
        }
    
        public void InsertWorkLog(List<TempoWorkLogs> worklogs)
        {
            //string cs = _config["connstr"];

            MySqlConnection jogetConn = null;


            #region JoGet Table Entry
            string jogetConnStr = @"server=localhost;database=jwdb;user=root;password=mySqlPass123$";


            try
            {
                jogetConn = new MySqlConnection(jogetConnStr);
                jogetConn.Open();

                MySqlCommand comm = jogetConn.CreateCommand();

                //-Updating duplicate record
                /*
                        INSERT INTO BLOGPOSTs ( postId, postTitle, postPublished)
                        VALUES(5, 'Python Tutorial', '2019-08-04')
                        ON DUPLICATE KEY UPDATE
                            postId = 5, postTitle = 'Python Tutorial', postPublished = '2019-08-04';
                */

                comm.CommandText = "INSERT INTO app_fd_worklogs(c_issueSummery,c_hours,c_issueKey) " +
                                   "VALUES(?c_issueSummery,?c_hours, ?c_issueKey,)";

                int i = 0;
                
                foreach (var log in worklogs)
                {

                    comm.Parameters.AddWithValue("?c_issueSummery", log.IssueDesc);
                    comm.Parameters.AddWithValue("?c_hours", log.BillableHrs);
                    comm.Parameters.AddWithValue("?c_issueKey", log.IssueKey);
                  

                    // comm.CommandText =$"INSERT INTO HubSpotDeals(PortalId,DealId,DealName,DealStage,Amount,LeadSource,CreatedDate,ClosedDate) VALUES({deal.PortalId}, {deal.DealId},{deal.Properties["DealName"]},{deal.Properties["DealStage"]},{deal.Properties["Amount"]},{deal.Properties["LeadSource"]},{deal.Properties["CreatedDate"]},{deal.Properties["ClosedDate"]})";

                    comm.ExecuteNonQuery();

                    comm.Parameters.Clear();

                    if (i == 0)
                        break;
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (jogetConn != null)
                {
                    jogetConn.Close();
                }

            }

            #endregion JoGet Table Entry


        }
    }
}
